<?php

namespace app\controllers;

use Yii;
use app\models\Pais;
use app\models\PaisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use mPDF;
use kartik\mpdf\Pdf;
use codemix\excelexport\ExcelFile;
use codemix\excelexport\ActiveExcelSheet;

use yii\grid\ActionColumn;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\grid\SerialColumn;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use arturoliveira\ExcelView;

/**
 * PaisController implements the CRUD actions for Pais model.
 */
class PaisController extends Controller
{
    


public function actionReport() {
    // get your HTML raw content without any layouts or scripts
    $content = $this->renderPartial('_reportView');
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Primer Reporte PDF'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Reporte PDF'], 
            'SetFooter'=>['{PAGENO}'],
            'SetTitle'=>["Primer REporte pdf"],
        ]
    ]);
    
    // return the pdf output as per the destination setting
    return $pdf->render(); 
}

public function actionReportepdf(){

     $searchModel = new PaisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      $content=  $this->renderPartial('index1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Primer Reporte'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Primer Reporte PDF'], 
            'SetFooter'=>['{PAGENO}'],
            'SetTitle'=>["Primer REporte pdf"],
        ]
    ]);

     
return $pdf->render(); 


}

public function actionReporteexel(){

 $searchModel = new PaisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
/*
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'id',
    'nombre',
    'codigo',
    'poblacion',
    ['class' => 'yii\grid\ActionColumn'],
];

 Renders a export dropdown menu
echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);
$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    'id',
    'nombre','codigo','poblacion',
    [

        'format'=>'raw'
    ],
];
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    
    'fontAwesome' => true,
    'batchSize' => 10,
    'target' => '_blank',
    //'folder' => '@webroot/tmp', // this is default save folder on server
]) . "<hr>\n".
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
]);
*/
    $model = Pais::find()->All();
    $filename = 'Data-'.Date('YmdGis').'-Reporte.xls';
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=".$filename);
    echo '<table border="1" width="100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>codigo</th>
                <th>Poblacion</th>
            </tr>
        </thead>';
        foreach($model as $data){
            echo '
                <tr>
                    <td>'.$data['id'].'</td>
                    <td>'.$data['nombre'].'</td>
                    <td>'.$data['codigo'].'</td>
                    <td>'.$data['poblacion'].'</td>
                </tr>
            ';
        }
    echo '</table>';


   
}























    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pais models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
public function actionApi(){

  \yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
 $searchModel = new PaisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model= Pais::find()->all();

   return array('data'=>$model);

}
    /**
     * Displays a single Pais model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pais model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pais();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pais model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pais model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pais model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pais the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pais::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.99');
    }
}
