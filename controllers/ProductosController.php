<?php

namespace app\controllers;

use app\models\Productos;
use app\models\ValidarFormularioProducto;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PaisSearch;
use app\models\ProductosSearch;
use yii\helpers\Html;
use yii\data\Pagination;
use yii\helpers\Url;

class ProductosController extends \yii\web\Controller
{
    public function actionIndex($mensaje=null)
    {
        return $this->render('index',["mensaje"=>$mensaje]);
    }


    public function actionFormulario($mensaje=null)
    {
    	return $this->render("formulario",["mensaje"=>$mensaje]);
    }


    public function actionRequest()
    {
    	$mensaje=null;
    	if(isset($_REQUEST["nombre"]))
    	{
    		$mensaje="Bien , primer formulario conectado con YII, enviaste: ".$_REQUEST["nombre"];
    	}

    	$this->redirect(["productos/formulario","mensaje"=>$mensaje]);

    }


   public function actionValidarformulario()
    {
    	$model= new ValidarFormulario;
    	if($model->load(Yii::$app->request->post()))
    		{
    			if ($model->validate()) {
    				//consultar en una BD ya q los datos fueron instroducidos correctamente
    			}
    			else{
    				$model->getErrors();
    			}
    		}
        return $this->render("validar",["model"=> $model]);
    }

public function actionProductos(){

  $model=new Productos;
  return $this->render("productos",["model"=>$model]);
}

public function actionCreate(){

    $model=new Productos;
    $mensaje=null;
  /*  if($model->load(yii::$app->request->post())){
        if($model->validate()){
            $table= new Productos;
            $table->codigo=$model->codigo;
            $table->nombre=$model->nombre;
            $table->cantidad=$model->cantidad;
            $table->precio=$model->precio;
            if ($table->insert()) {

                $mensaje="Datos guardados en la BD";
                $model->codigo=null;
                $model->nombre=null;
                $model->cantidad=null;
                $model->precio=null;
            }
            else{
                $mensaje="ha ocurrido un error";
            }
        }
    }*/
    if($model->load(yii::$app->request->post())){

        if($model->validate()) {
          $model->save();
           $mensaje="Datos guardados en la BD";
                $model->codigo=null;
                $model->nombre=null;
                $model->cantidad=null;
                $model->precio=null;
        
        $mensaje="GUARDADO CON EL METODO SAVE";}
      
    }


	return $this->render("create",["model"=>$model,"mensaje"=>$mensaje]);
}


/*public function actionVer(){
     $search=null;
    $table=new Productos;
    $model=$table->find()->all();

    $form=new ProductosSearch;
   
    if ($form->load(yii::$app->request->get())) {

        if($form->validate()){

            $search= Html::encode($form->q);
            $query=" SELECT*FROM productos WHERE codigo LIKE '%$search%' OR nombre LIKE '%$search%' ";
            $model=$table->findBySql($query)->all();
        }
        else {
            $form=getErrors();
        }
    }




    return $this->render("ver",["model"=>$model,"form"=>$form,"search"=>$search]);
}*/

public function actionVer(){

    $search=null;
    $form=new ProductosSearch;

    if ($form->load(yii::$app->request->get())){

        if ($form->validate()) {

              $search= Html::encode($form->q);
              $table= Productos::find()->where(["Like","codigo",$search])
              ->orWhere(["like","nombre",$search]);

              $count = clone $table;
              $pages= new Pagination(["pageSize"=>1,"totalCount"=>$count->count()]);


              $model= $table->offset($pages->offset)->limit($pages->limit)->all();
        } else {
            $form=getErrors();
        }
    }
    else{
        $table= Productos::find();
        $count= clone $table;

        $pages=new Pagination(["pageSize"=>1,"totalCount"=>$count->count()]);

           $model= $table->offset($pages->offset)->limit($pages->limit)->all();
    }

    return $this->render("ver",["model"=>$model,"form"=>$form,"search"=>$search,"pages"=>$pages]);
}


public function actionApi(){

  \yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
   $table= Productos::find();

   return array('data'=>$table);

}


public function actionBuscar(){

    $form=new ProductosSearch;
    $search=null;
    if($form->load(yii::$app->request->get())){

        if($form->validate()){

            $search= Html::encode($form->q);
            $query="select*from productos where nombre like '%$search%' or codigo like '%$search%'";
            $form= $table->findBySql($query)->all();
        }
        else
        {
            $form=getErrors();
        }


    }

    return $this->render("ver",["form"=>$form,"$search"=>$search]);

}




  public function actionActualizar()
    {
       $model= new Productos;
       $mensaje=null;
       if ($model->load(yii::$app->request->post())) 
        {
           if ($model->validate())
            {
               $table=Productos::findOne($model->codigo);
               if ($table) 
               {

                    $table->nombre=$model->nombre;
                    $table->cantidad=$model->cantidad;
                    $table->precio=$model->precio;
                   if($table->update()){
                   $mensaje="Actualizado con el metodo update";}
                   else{
                    $mensaje="ni idea q mierda pasa";
                   }
               }
               else
               {
                $mensaje ="el Producto no ha sido encontrado";
               }

           }
           else
           {
            $model->getErrors();
           }
       }


       if (yii::$app->request->get("codigo")) {

          $codigo= Html::encode($_GET["codigo"]);

            $table= Productos::findOne($codigo);
            if($table){

                $model->codigo=$table->codigo;
                $model->nombre=$table->nombre;
                $model->cantidad=$table->cantidad;
                $model->precio=$table->precio;

            }
            else{
                return redirect(["productos/ver"]);
            }
       }
       else
       {
        return $this->redirect(["productos/ver"]);
       }

        return $this->render("actualizar",["model"=>$model,"mensaje"=>$mensaje]);
    }






    public function actionBorrar(){

        if(yii::$app->request->post()){

            $codigo= Html::encode($_POST["codigo"]);

            if(Productos::deleteAll("codigo=:codigo",[":codigo"=>$codigo])){

                 echo "eliminado , redireccionando ...";
                    echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("productos/ver")."'>"; 


            }else{
                echo "Ha ocurrido un error al eliminar el producto, redireccionando ...";
                    echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("productos/ver")."'>"; 
            }

        }else{
            $this->redirect(["productos/ver"]);
        }


          
    }
















 protected function findModel($codigo)
    {
        if (($model = Productos::findOne($codigo)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.99');
    }

}
