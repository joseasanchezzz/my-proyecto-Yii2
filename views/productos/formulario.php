<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<h1>Formulario</h1>
<?= Html::beginForm(
	Url::toRoute("productos/request"), 
	"post", 
	["class"=> "form-inline"]);
?>
<div class="form-group">
	<?= Html::label("Ingresa tu Nombre","nombre")?>
	<?= Html::textInput("nombre",null,["class"=>"form-control"])?>
	</div>
	<?= Html::submitInput( "Enviar Nombre",["class"=> "btn btn-primary"])?>
	
	<h2><?= $mensaje ?></h2>

<?= Html::endForm()?>