<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$this->title = 'actualizar';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<a href="<?= Url::toRoute("productos/ver") ?>">Ir a la lista de Productos</a>

<h1>Editar Producto con el codigo <?= Html::encode($_GET["codigo"]) ?></h1>

<h3><?= $mensaje ?></h3>

<?php $form = ActiveForm::begin([
    "method" => "post",
    'enableClientValidation' => true,
]);
?>



<div class="form-group">
 <?= $form->field($model, "codigo")->input("text") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "nombre")->input("text") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "cantidad")->input("number") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "precio")->input("number") ?>   
</div>

<?= Html::submitButton("Actualizar", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>