<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use yii\widgets\LinkPager;
$this->title = 'Ver';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

 ?>

<h3>Buscar Producto</h3>
<?php $f= ActiveForm::begin(["method"=>"get","action"=> Url::toRoute("productos/ver"),"enableClientValidation"=>true,]) ?>
<div class="form-group">

	<?= $f->field($form,"q")->input("search") ?>
	
</div>
<?= Html::submitButton("Buscar",["class"=>"btn btn-primary"]) ?>


<?php $f->end()   ?>



 <h2><a href="<?= Url::toRoute("productos/create") ?>">Registrar un Producto   </a></h2>

 <h1>Lista de Productos</h1>

<table class="table table-bordered">
 <tr>
    <th>Codigo </th>
    <th>Nombre </th>
    <th>Cantidad </th>
    <th>Precio  </th>
    <th></th>
    <th></th>
 </tr>
<?php foreach ($model as $row): ?>
<tr>
    <td><?= $row->codigo ?></td>
    <td><?= $row->nombre ?></td>
    <td><?= $row->cantidad ?></td>
    <td><?= $row->precio ?></td>
    <td><a href="<?= Url::toRoute(["productos/actualizar","codigo"=>$row->codigo]) ?>">Editar</a></td>
    <td> 
        <a href="#" data-toggle="modal" data-target="#codigo_<?= $row->codigo ?>">Eliminar</a>
            <div class="modal fade" role="dialog" aria-hidden="true" id="codigo_<?= $row->codigo ?>">
                      <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Eliminar Producto</h4>
                              </div>
                              <div class="modal-body">
                                    <p>¿Realmente deseas eliminar Producto con el codigo <?= $row->codigo ?>?</p>
                              </div>
                              <div class="modal-footer">
                              <?= Html::beginForm(Url::toRoute("productos/borrar"), "POST") ?>
                                    <input type="hidden" name="codigo" value="<?= $row->codigo ?>">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Eliminar</button>
                              <?= Html::endForm() ?>
                              </div>
                            </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    </td>
</tr>
<?php endforeach ?>

 </table>

 <h3><?=$search ?></h3>

 <?= LinkPager::widget(["pagination"=>$pages])?>